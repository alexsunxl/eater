/**
*	指令 百度地图
*/

angular
	.module('starter.directive')
	.directive('baiduMap',baiduMap);


function baiduMap(){
	var directive = {
        link: link,
        restrict: 'EA',
        $scope:{
        	myPosition:"=myPosition"
        },
        controller:controller
    };
    return directive;

    function controller($scope){

    }

    function link($scope, element, attrs, controller) {
    	$scope.id = attrs.id;


    	//监控 文字填写框的变化
    	$scope.$watch(function(){
    		return $scope.myPosition
    	},function(newValue, oldValue){
			local.search(newValue);
    	});




		// 创建地图
		var map = new BMap.Map("allmap");    // 创建Map实例
		map.centerAndZoom(new BMap.Point(116.404, 39.915), 16);  // 初始化地图,设置中心点坐标和地图级别
		// map.addControl(new BMap.MapTypeControl());   //添加地图类型控件
		map.setCurrentCity("北京");          // 设置地图显示的城市 此项是必须设置的
		map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放

		//搜索模块的选项
		var options = {
			onSearchComplete: function(results){
				// 判断状态是否正确
				if (local.getStatus() == BMAP_STATUS_SUCCESS){
					var s = [],title,address,resulte_row,
						results_length = results.getCurrentNumPois(),
						results_wrap = document.getElementById("results");

					if(results_length > 0 ){
						results_wrap.style.display = "block";
					}

					for (var i = 0; i < results_length; i ++){
						//搜索结果 小于5个
						if(i == 5) break;

						title = "<span class='result-item-title'>"+results.getPoi(i).title+"</span>";
						address = "<span class='result-item-address'>"+results.getPoi(i).address+"</span>";

						resulte_row = "<div class='result-itme-row'>"+title + address+"</div>"

						s.push(resulte_row);
					}
					results_wrap.innerHTML = s.join("<br/>");
				}
			}
		};
		var local = new BMap.LocalSearch(map, options);


		//地图定位
		var geolocation = new BMap.Geolocation();
		geolocation.getCurrentPosition(function(r){
			if(this.getStatus() == BMAP_STATUS_SUCCESS){
				var mk = new BMap.Marker(r.point);
				map.addOverlay(mk);
				map.panTo(r.point);
				console.log('您的位置：'+r.point.lng+','+r.point.lat);
			}
			else {
				console.log('failed'+this.getStatus());
			}        
		},{enableHighAccuracy: true});

    }

}